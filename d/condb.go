package condb

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"

	"gitlab.com/zendrulat123/fs/d/grabdb"
)

func Dber(table interface{}) {
	db, err := grabdb.GetCon()
	if err != nil {
		fmt.Println("db connect not good", err)
	}
	spew.Dump(table)
	db.AutoMigrate(table)

	result := db.Create(table) // pass pointer of data to Create
	fmt.Println(result.Error, result.RowsAffected)
	defer db.Close()

}
