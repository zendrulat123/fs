package grabdb

import (
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
)

func GetCon() (*gorm.DB, error) {
	viper.SetConfigName("database") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)

	}

	// Declare var root:password@tcp(127.0.0.1:3307)/app
	// dialect := viper.GetString("development.dialect")
	// DS := viper.GetString("development.datasource")
	fmt.Println(viper.GetString("database.datasource"))
	db, err := gorm.Open("mysql", "root:password@(127.0.0.1:3307)/app?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println(err)
	}

	os.Stat(viper.GetString("database.dir"))
	if err != nil {
		fmt.Println(err)
	}

	return db, nil
}
