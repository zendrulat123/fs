module gitlab.com/zendrulat123/fs

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/gorm v1.9.16
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mattn/go-sqlite3 v1.14.5 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rs/cors v1.7.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.8
)
