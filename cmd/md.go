package cmd

import (
	"fmt"
	"html/template"
	"log"
	"os"
	"os/exec"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	com "gitlab.com/zendrulat123/fs/c"
)

// mdCmd represents the md command
var mdCmd = &cobra.Command{
	Use:     "md",
	Short:   "A brief description of your command",
	Long:    `running cmd`,
	Example: `go run *.go md -m "person" -p "name.string-"jim" age.int-33"`,
	Run: func(cmd *cobra.Command, args []string) {
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath("con/") // config file path
		viper.AutomaticEnv()        // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		path := viper.GetString("env.path")
		fn := viper.GetString("env.prj")
		model, _ := cmd.Flags().GetString("model")
		prop, _ := cmd.Flags().GetString("prop")
		ds := viper.GetString("env.db")
		getpath(path, fn, model, prop, ds)
	},
}

func init() {
	rootCmd.AddCommand(mdCmd)
	mdCmd.Flags().StringP("model", "m", "", "Set your model")
	mdCmd.Flags().StringP("prop", "p", "", "Set your properties")
}

func getpath(path string, fn string, model string, prop string, ds string) {
	com.MkF(path, fn)
	type data struct {
		Model     string
		Prop      []string
		Propnodot []string
		Ds        string
	}
	var str string
	var strnodot string
	var sd []string
	var property []string
	var strprop string
	var sdd []string
	//allows for database column "name.datatype" command
	//basically to get from "name.string-jim age.int-33" to a slice of these values
	//https://play.golang.org/p/O9yIZUUT6_S
	s := strings.Split(prop, " ")
	for _, ss := range s {
		strnodot = com.TrimStringFromDot(ss)
		property = append(property, strnodot)
		str = strings.Replace(ss, ".", " ", 1)
		sd = append(sd, str)
	}
	for _, ss := range sd {
		strprop = strings.Replace(ss, "-", " ", 1)
		sdd = append(sdd, strprop)
	}
	fmt.Println(sdd)

	exec.Command("go get -u gorm.io/gorm")
	exec.Command("go get -u gorm.io/driver/sqlite")
	tm := template.Must(template.New("queue").Parse(queueTemplate))
	//get path for template execution
	f, err := os.Create(path + "db" + fn)
	if err != nil {
		log.Println("create file: ", err)
		return
	}
	//loading template vars
	Cm := strings.Title(model)
	var d = data{Model: Cm, Prop: sd, Propnodot: property, Ds: ds}
	err = tm.Execute(f, d)
	if err != nil {
		log.Print("execute: ", err)
		return
	}
	exec.Command("gofmt")
	exec.Command("goimports")
	exec.Command("go build")
	f.Close()
}

var queueTemplate = `
  package main
  
  import (
	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
  )
  
  type {{.Model}} struct {
	{{range slice .Prop 0 }}
	{{.}} 
	{{end}}
  }

  func {{.Model}}DB(){

  db, err := gorm.Open(mysql.Open(\"{{.Ds}}"), &gorm.Config{})
  if err != nil {
	  panic("failed to connect database")
  }

  db.AutoMigrate(&{{.Model}}{})

  // Create
  db.Create(&{{.Model}}{	{{range slice .Propnodot 0 }}
	{{. }} : 
	{{end}}})

  db.Create(&{{.Model}}{id:1})
  db.Find(&{{.Model}}, "id = ?", 1)
  }
  `
