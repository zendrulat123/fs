package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	com "gitlab.com/zendrulat123/fs/c"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:     "cr",
	Short:   "Creates the config file so that your project has persistant data",
	Long:    `You need the config to allow for the rest of the commands to know about things (i.e. path, name, anything else you want)`,
	Example: `go run *.go cr -p /serv -n projectname -d "root:@tcp(127.0.0.1:3306)/ap"`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("create called")
		path, _ := cmd.Flags().GetString("path")
		name, _ := cmd.Flags().GetString("model")
		db, _ := cmd.Flags().GetString("db")
		//check if file exists
		if _, err := os.Stat("con/persis.yaml"); err == nil {
			fmt.Printf("File exists so nothing will be overriden \n")
		} else {
			fmt.Printf("File does not exist\n")
			com.CMT()
			com.WriteFile("con/persis.yaml")
			com.AddE("con/persis.yaml", "env:")
			com.AddV("con/persis.yaml", " path: "+"\""+path+"/"+"\"")
			com.AddV("con/persis.yaml", " prj: "+"\""+name+".go"+"\"")
			com.AddV("con/persis.yaml", " db: "+"\""+db+"\"")
		}
		//create config dir/file
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath(".")
		viper.AddConfigPath("./con/") // config file path
		viper.AutomaticEnv()          // read value ENV variable

		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}

	},
}

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.Flags().StringP("path", "p", "", "Set your path")
	createCmd.Flags().StringP("model", "n", "", "Set your project name")
	createCmd.Flags().StringP("db", "d", "", "Set your database source")
}
