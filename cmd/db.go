/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// dbCmd represents the db command
var dbCmd = &cobra.Command{
	Use:   "db",
	Short: "A brief description of your command",
	Long:  `db`,
	Run: func(cmd *cobra.Command, args []string) {
		// var err error
		fmt.Println("db called")
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")

		viper.AddConfigPath("con/") // config file path
		viper.AutomaticEnv()        // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		DS := viper.GetString("env.db")
		// path := viper.GetString("env.path")
		// fn := viper.GetString("env.prj")
		fmt.Println(DS)

	},
}

func init() {
	rootCmd.AddCommand(dbCmd)
}
func GetConn() {

}
