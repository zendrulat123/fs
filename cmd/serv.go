package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	com "gitlab.com/zendrulat123/fs/c"
	serv "gitlab.com/zendrulat123/fs/c/servs"
)

var servCmd = &cobra.Command{
	Use:     "serv",
	Short:   "Creates the server files and main.go",
	Long:    `a`,
	Example: `go run *.go serv -m "person" -t ":8081"`,

	Run: func(cmd *cobra.Command, args []string) {
		viper.SetConfigName("persis") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath("con/") // config file path
		viper.AutomaticEnv()        // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}

		path := viper.GetString("env.path")
		filename := viper.GetString("env.prj")
		fmt.Println("this is path : ", path, "this is the filename: ", filename)
		model, _ := cmd.Flags().GetString("model")
		port, _ := cmd.Flags().GetString("port")
		file, mfile, tfile, dir := com.MKT(path, filename)
		serv.Servtemp(port, model, file, mfile, tfile, dir)
	},
}

func init() {
	rootCmd.AddCommand(servCmd)
	servCmd.Flags().StringP("model", "m", "", "Set your model")
	servCmd.Flags().StringP("port", "t", "", "Set your port")
}
