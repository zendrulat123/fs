fs
	Use:        "serv",
	Aliases:    []string{},
	SuggestFor: []string{},
	Short:      "A brief description of your command",
	Long:       `a`,
	Example:    "",
	ValidArgs:  []string{},
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	},
	Args: func(cmd *cobra.Command, args []string) error {
	},
	ArgAliases:             []string{},
	BashCompletionFunction: "",
	Deprecated:             "",
	Annotations:            map[string]string{},
	Version:                "",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
	},
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
	},
	PreRun: func(cmd *cobra.Command, args []string) {
	},
	PreRunE: func(cmd *cobra.Command, args []string) error {
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("serv called")
		path, _ := cmd.Flags().GetString("path")
		model, _ := cmd.Flags().GetString("model")
		port, _ := cmd.Flags().GetString("port")
		spew.Dump(path, model, port)
		genserv(path, model, port)
	},
	RunE: func(cmd *cobra.Command, args []string) error {
	},
	PostRun: func(cmd *cobra.Command, args []string) {
	},
	PostRunE: func(cmd *cobra.Command, args []string) error {
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
	},
	PersistentPostRunE: func(cmd *cobra.Command, args []string) error {
	},
	FParseErrWhitelist:         cobra.FParseErrWhitelist{},
	TraverseChildren:           false,
	Hidden:                     false,
	SilenceErrors:              fals